use actix_cors::Cors;
use actix_web::{http, post, web, App, HttpResponse, HttpServer, Responder};

use hyper::body::HttpBody as _;
use hyper::{Client, Uri};
use hyper_tls::HttpsConnector;
use regex::Regex;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct OgpData {
    domain: String,
    image: String,
    title: String,
    description: String,
    url: String,
}

impl OgpData {
    fn new() -> Self {
        Self {
            domain: String::from(""),
            image: String::from(""),
            title: String::from(""),
            description: String::from(""),
            url: String::from(""),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OgpInfo {
    url: String,
}

#[derive(Serialize, Deserialize)]
pub struct HttpTest(String);

async fn get_head(url: String) -> String {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let uri = url.parse::<Uri>().unwrap();

    let mut resp = client.get(uri).await.unwrap();

    let mut lines = "".to_string();
    while let Some(chunk) = resp.body_mut().data().await {
        if let Ok(bytes) = chunk {
            let line = String::from_utf8_lossy(&bytes).to_string();

            let re = Regex::new(r"([\s\S]*?</head>)").unwrap();

            if re.captures_iter(&line).count() >= 1 {
                let caps = re.captures_iter(&line).next().unwrap();
                lines += caps.get(1).unwrap().as_str();
                break;
            } else {
                lines += &line;
                if lines.contains("</head>") {
                    break;
                }
            }
        }
    }
    lines
}

pub async fn get_ogps(url: String) -> OgpData {
    let lines = get_head(url).await;
    // println!("lines = {}\n", lines);

    // meta タグを取り出す正規表現
    let re_meta = Regex::new(r#"(<meta[\s\S]*?>)"#).unwrap();

    // meta タグ内の property を取り出す正規表現
    let re_property = Regex::new(r#"property=['"]([\s\S]*?)['"]"#).unwrap();

    // meta タグ内の content を取り出す正規表現
    let re_content = Regex::new(r#"content=['"]([\s\S]*?)['"]"#).unwrap();

    let re_domain = Regex::new(r#"http.://(www.)?(?P<domain>.*?)(/|$)"#).unwrap();

    let mut ogp_data = OgpData::new();

    for caps in re_meta.captures_iter(lines.as_str()) {
        let meta_tag = caps.get(1).unwrap().as_str();
        if meta_tag.contains("og:")
            && meta_tag.contains("property=")
            && meta_tag.contains("content=")
        {
            // println!("meta: {}", meta_tag);
            let property = re_property
                .captures_iter(meta_tag)
                .next()
                .unwrap()
                .get(1)
                .unwrap()
                .as_str();

            let content = re_content
                .captures_iter(meta_tag)
                .next()
                .unwrap()
                .get(1)
                .unwrap()
                .as_str();

            match property {
                "og:url" => {
                    let domain = re_domain
                        .captures_iter(&content)
                        .next()
                        .unwrap()
                        .name("domain")
                        .unwrap()
                        .as_str()
                        .to_string();
                    ogp_data.domain = domain;

                    ogp_data.url = content.to_string();
                }
                "og:image" => {
                    ogp_data.image = content.to_string();
                }
                "og:title" => {
                    ogp_data.title = content.to_string();
                }
                "og:description" => {
                    ogp_data.description = content.to_string();
                }
                _ => {}
            }
        }
    }

    ogp_data
}

#[post("/get_ogps")]
async fn post_get_ogps(ogp_info: web::Json<OgpInfo>) -> impl Responder {
    let ogps = get_ogps(ogp_info.url.clone()).await;
    let obj = serde_json::to_string(&ogps).unwrap();
    HttpResponse::Ok().body(obj)
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        // https://kouya17.com/posts/38/
        let cors = Cors::default()
            .allowed_origin("http://127.0.0.1:1111")
            .allowed_origin("https://pellpell.net")
            .allowed_methods(vec!["POST"])
            .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
            .allowed_header(http::header::CONTENT_TYPE);

        App::new().wrap(cors).service(post_get_ogps)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
