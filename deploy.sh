#!/bin/bash

PROJECT_DIR=$(cd $(dirname $0); pwd)
source $PROJECT_DIR/.env

gcloud config configurations activate $PROJECT_ID

gcloud builds submit --tag gcr.io/$PROJECT_ID/pellpell-home-backend:1.0.0 $PROJECT_DIR --timeout 3600

gcloud run deploy pellpell-home-backend \
    --image=gcr.io/$PROJECT_ID/pellpell-home-backend:1.0.0 \
    --platform managed \
    --region=asia-northeast1
